// backend
const backendUrl = process.env.REACT_APP_BE_URL;

//post a new ineligible period to backend DB
export const addIneligiblePeriod = async (token, ineligibleSelectionObject) => {
  try {
    const authString = `Bearer ${token}`;
    const startString = ineligibleSelectionObject.start;
    const endString = ineligibleSelectionObject.end;

    const response = await fetch(`${backendUrl}/api/ineligible`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        periodStart: startString,
        periodEnd: endString,
      }),
    });
    if (!response.ok) {
      throw new Error(
        "Could not add request, TODO add status code to feedback"
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//get all ineligible periods
export const getIneligibles = async (token) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/ineligible`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not fetch requests, status code is " + response.status
      );
    }
    if (response.status === 204) {
      return [[], null];
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [error.message, null];
  }
};

//delete an ineligible
export const deleteIneligibleById = async (token, ineligibleId) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(
      `${backendUrl}/api/ineligible/${ineligibleId}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
      }
    );
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not delete request, status code is " + response.status
      );
    }
    //delete just returns the status
    return [response.status, null];
  } catch (error) {
    return [error.message, null];
  }
};
