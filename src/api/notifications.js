import { notificationTypeComment, notificationTypeRequest } from "../const/notificationTypes";

const backendUrl = process.env.REACT_APP_BE_URL;

//get all notifications for this user
export const getNotifications = async (token, userId) => {
    try {
      const authString = `Bearer ${token}`;
      const response = await fetch(`${backendUrl}/api/user/${userId}/notifications`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
      });
      if (!response.ok) {
        throw new Error(
          "ERROR: Could not fetch notifications, status code is " + response.status
        );
      }
      if (response.status === 204) {
        return [[], null];
      }
      const result = await response.json();
      return [result, null];
    } catch (error) {
      return [error.message, null];
    }
  };

  //update seen status of a request/comment to approved or denied
export const updateNotificationContentStatus = async (token, contentType, contentId, seen) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/notification/${contentType}/${contentId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body:
        seen,
    });
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not update request, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};