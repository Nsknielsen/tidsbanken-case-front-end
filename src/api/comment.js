// backend
const backendUrl = process.env.REACT_APP_BE_URL;

//get request by id
export const getCommentsByRequestId = async (token, requestId) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(
      `${backendUrl}/api/request/${requestId}/comment`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
      }
    );
    //403 is a special case where we dont want to show an error in GUI
    //because the user is non-admin and should not see other people's comments
    if (response.status === 403) {
      return [null, null];
    }
    if (!response.ok) {
      throw new Error(
        "Could not fetch request, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//add a new comment
export const addComment = async (token, requestId, commentMessage) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(
      `${backendUrl}/api/request/${requestId}/comment`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
        body: JSON.stringify({
          message: commentMessage,
        }),
      }
    );
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not add new comment, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};
