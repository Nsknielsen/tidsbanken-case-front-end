const backendUrl = process.env.REACT_APP_BE_URL;

//post a new request to backend DB
export const addRequest = async (
  token,
  requestSelection,
  requestTitle,
  requestComment
) => {
  try {
    const authString = `Bearer ${token}`;
    const startString = requestSelection.start;
    const endString = requestSelection.end;

    const response = await fetch(`${backendUrl}/api/request`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        title: requestTitle,
        periodStart: startString,
        periodEnd: endString,
      }),
    });
    const postRequestResult = await response.json();

    if (!response.ok) {
      throw new Error(
        "ERROR: Could not add request, status code is " + response.status
      );
    } else {
      //add initial request comment to db
      const requestId = postRequestResult.id;
      const response = await fetch(
        `${backendUrl}/api/request/${requestId}/comment`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: authString,
          },
          body: JSON.stringify({
            message: requestComment,
          }),
        }
      );
      const postCommentResult = await response.json();
      if (!response.ok) {
        throw new Error(
          "ERROR: Could not add requestComment, status code is " +
            response.status
        );
      }
    }
    return [postRequestResult, null];
  } catch (error) {
    return [null, error.message];
  }
};

//get all requests
export const getRequests = async (token, page) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/request?&page=${page}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not fetch requests, status code is " + response.status
      );
    }
    if (response.status === 204) {
      return [[], null];
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [error.message, null];
  }
};

//get request by id
export const getRequestById = async (token, requestId) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/request/${requestId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not fetch request, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//update requestStatus of a request to approved or denied
export const updateRequestStatus = async (token, requestId, requestStatus) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/request/${requestId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        requestStatus,
      }),
    });
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not update request, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//update details of a request (title, period)
export const updateRequestDetails = async (
  token,
  requestId,
  requestSelection,
  requestTitle
) => {
  try {
    const authString = `Bearer ${token}`;
    const startString = requestSelection.start;
    const endString = requestSelection.end;
    const response = await fetch(`${backendUrl}/api/request/${requestId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        title: requestTitle,
        periodStart: startString,
        periodEnd: endString,
      }),
    });
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not update request, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//delete a request
export const deleteRequestById = async (token, requestId) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/request/${requestId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "ERROR: Could not delete request, status code is " + response.status
      );
    }
    //delete just returns the status
    return [response.status, null];
  } catch (error) {
    return [error.message, null];
  }
};
