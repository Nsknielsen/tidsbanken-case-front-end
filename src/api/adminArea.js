//JS file for the relevant api calls to make in admin area when creating or updating a user.

// keycloak
const keycloakAdminTokenUrl =
  process.env.REACT_APP_KEYCLOAK_ADMIN_API_TOKEN_URL;
const keycloakAdminResourceUrl =
  process.env.REACT_APP_KEYCLOAK_ADMIN_API_RESOURCE_URL;
const keycloakAdminUsername = process.env.REACT_APP_KEYCLOAK_ADMIN_USERNAME;
const keycloakAdminPassword = process.env.REACT_APP_KEYCLOAK_ADMIN_PASSWORD;

// backend
const backendUrl = process.env.REACT_APP_BE_URL;

//function to get the access Keycloak token from admin console
export const fetchKeycloakAdminToken = async () => {
  try {
    const response = await fetch(`${keycloakAdminTokenUrl}`, {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: new URLSearchParams({
        grant_type: "password",
        client_id: "admin-cli",
        username: keycloakAdminUsername,
        password: keycloakAdminPassword,
      }),
    });
    if (!response.ok) {
      throw new Error(
        "Could not fetch keycloak token, , status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//post a new Keycloak user to keycloak
export const addKeycloakUser = async (userObject, password, isAdmin) => {
  try {
    //add admin role mapping (through a one-role "admin" group, a hack to handle the bad Keycloak API documentation)
    if (isAdmin) {
      userObject.groups = ["admin"];
    }
    const [token, userError] = await fetchKeycloakAdminToken();
    const authString = `Bearer ${token.access_token}`;
    const response = await fetch(`${keycloakAdminResourceUrl}/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        enabled: true,
        ...userObject,
      }),
    });
    if (!response.ok) {
      throw new Error(
        "Could not add user to Keycloak, status code is " + response.status
      );
    }
    //get new userId
    const userUrl = response.headers.get("location");
    const userId = userUrl.split("users/")[1];
    //set password
    const [setPasswordResponse, passwordError] = await setKeycloakUserPassword(
      userId,
      password
    );
    if (!setPasswordResponse.ok) {
      throw new Error(
        "Could not add user to Keycloak, status code is " + response.status
      );
    }
    return [response, null];
  } catch (error) {
    return [null, error.message];
  }
};

//update/set a Keycloak user password
export const setKeycloakUserPassword = async (userId, password) => {
  try {
    const [token, error] = await fetchKeycloakAdminToken();
    const authString = `Bearer ${token.access_token}`;
    const response = await fetch(
      `${keycloakAdminResourceUrl}/users/${userId}/reset-password`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
        body: JSON.stringify({
          value: password,
          temporary: true,
        }),
      }
    );
    if (!response.ok) {
      throw new Error(
        "Could not set password in keycloak, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//post a new BE DB user to keycloak
export const addBackendUser = async (userObject, isAdmin, token) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/user`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        email: userObject.email,
        admin: isAdmin,
        firstName: userObject.firstName,
        lastName: userObject.lastName,
        id: userObject.id,
      }),
    });
    if (!response.ok) {
      throw new Error(
        "Could not add user to backend, status code is " + response.status
      );
    }

    return [response, null];
  } catch (error) {
    return [null, error.message];
  }
};

//get all users from backend
export const getAllBackendUsers = async (token) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/user/all`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "Could not fetch users, status code is " + response.status
      );
    }
    if (response.status === 204) {
      return [[], null];
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

export const getAllKeycloakUsers = async () => {
  try {
    const [token, userError] = await fetchKeycloakAdminToken();
    const authString = `Bearer ${token.access_token}`;
    const response = await fetch(`${keycloakAdminResourceUrl}/users`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "Could not get users from Keycloak, status code is " + response.status
      );
    }
    const result = await response.json();
    for (let user of result) {
      const userGroups = await getKeycloakUserGroups(user.id);
      if (userGroups[0].length > 0 && userGroups[0][0].name === "admin") {
        user.isAdmin = true;
      } else {
        user.isAdmin = false;
      }
    }
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

export const getKeycloakUserGroups = async (userId) => {
  try {
    const [token, userError] = await fetchKeycloakAdminToken();
    const authString = `Bearer ${token.access_token}`;
    const response = await fetch(
      `${keycloakAdminResourceUrl}/users/${userId}/groups`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: authString,
        },
      }
    );
    if (!response.ok) {
      throw new Error(
        "Could not get user groups from Keycloak, status code is " +
          response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//patch the admin status of a user in  backend - added to avoid refactoring the user-oriented patch in profile.js for now
export const updateBackendAdminStatus = async (isAdmin, idUser, token) => {
  try {
    console.log('updating admin', isAdmin)
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/user/${idUser}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
      body: JSON.stringify({
        admin: isAdmin,
      }),
    });
    if (!response.ok) {
      throw new Error("Could not patch user, status code is" + response.status);
    }
    const result = await response.json();
    console.log(result, 'result')
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};
