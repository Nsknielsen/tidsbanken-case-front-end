//used for user access in relation to comments, unlike the keycloak user access in adminArea.js
const backendUrl = process.env.REACT_APP_BE_URL;

//get user by url
export const getUserByUrl = async (token, userUrl) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}${userUrl}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "Could not fetch user, status code is " + response.status
      );
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};

//get user by id
export const getUserById = (token, idUser) =>
  getUserByUrl(token, `/api/user/${idUser}`);

//get request by user id
export const getRequestsByUserId = async (token, idUser) => {
  try {
    const authString = `Bearer ${token}`;
    const response = await fetch(`${backendUrl}/api/user/${idUser}/requests`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: authString,
      },
    });
    if (!response.ok) {
      throw new Error(
        "Could not fetch request, status code is " + response.status
      ); // todo: status code
    }
    if (response.status === 204) {
      return [[], null];
    }
    const result = await response.json();
    return [result, null];
  } catch (error) {
    return [null, error.message];
  }
};
