import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { ineligibleReducer } from "./ReduxState/IneligibleState";
import { viewRequestGuiReducer } from "./ReduxState/ViewRequestGuiState";
import { viewRequestReducer } from "./ReduxState/ViewRequestState";

//combine all reducers into one
const appReducers = combineReducers({
  viewRequest: viewRequestReducer,
  viewRequestGui: viewRequestGuiReducer,
  ineligiblePeriods: ineligibleReducer,
});

//register ALL reducers and middleware in our state and expose it
export default createStore(
  appReducers,
  composeWithDevTools(applyMiddleware())
);
