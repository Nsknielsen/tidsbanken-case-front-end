import { useKeycloak } from "@react-keycloak/web";
import { Navigate } from "react-router-dom";

//wrapper component to put around routes in app.js that we want to protect.
//might be superfluous, but maybe still has niche use in case of session timeouts where links are visible but should no longer be valid
const PrivateRoute = ({ children }) => {
  const { keycloak } = useKeycloak();

  const isLoggedIn = keycloak.authenticated;

  //return children if logged in, otherwise redirect to homepage where user can login
  return isLoggedIn ? children : <Navigate replace to="/" />;
};

export default PrivateRoute;
