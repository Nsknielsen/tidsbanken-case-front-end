import { useKeycloak } from "@react-keycloak/web";
import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { getRequestById } from "../../api/request";
import { getRequestsByUserId } from "../../api/user";
import "../../tables.css"

const RequestList = () => {
  //keycloak access to conditionally render based on admin/not admin
  const { keycloak, initialized } = useKeycloak();

  //local state to hold information about success and result content
  const [getRequestsResponse, setGetRequestsResponse] = useState(null);
  const [getRequestResponse, setGetRequestResponse] = useState(null);
  const [requestList, setRequestList] = useState([]);

  //useeffect set up to run once on initial mount
  useEffect(() => {
    handleLoadRequests();
  }, []);

  //load the requests to show
  const handleLoadRequests = async () => {
    // get token
    const token = keycloak.token;
    const idUser = keycloak.tokenParsed.sub;
    const [resultAllRequests, error] = await getRequestsByUserId(token, idUser);
    if (resultAllRequests) {
      let requestArray = [];
      for (let requestUrl of resultAllRequests) {
        const idRequest = requestUrl.split("request/")[1];
        const [resultRequest, error] = await getRequestById(token, idRequest);
        if (resultRequest) {
          requestArray.push(resultRequest);
        } else {
          setGetRequestResponse(error);
        }
      }
      setRequestList(requestArray);
    } else {
      setGetRequestsResponse(error);
    }
  };

  // navigator used to navigate to details view
  const navigate = useNavigate();
  return (
    <div className="dv-requestlist">
      {getRequestsResponse}
      {getRequestResponse}
      <div>
        <h5>Request History</h5>
        {requestList.length == 0 && <i>No requests found</i>}
        {requestList.length > 0 && (
          <>
            <i>
              <div>&nbsp;</div>Click on a request for more details
              <div>&nbsp;</div>
            </i>
            <Table>
              <tbody className="t-body">
                <tr className="t-r-header" key={"header"}>
                  <th>Title</th>
                  <th>Start</th>
                  <th>End</th>
                  <th>Status</th>
                </tr>
                {requestList.map((request) => {
                  return (
                    <tr className="t-r-request"
                      key={request.id}
                      onClick={() => navigate(`/request/${request.id}`)}
                    >
                      <td>{request.title}</td>
                      <td>
                        {new Date(request.periodStart).toString().slice(0, 16)}
                      </td>
                      <td>
                        {new Date(request.periodEnd - 1)
                          .toString()
                          .slice(0, 16)}
                      </td>
                      <td>{request.requestStatus}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </>
        )}
      </div>
    </div>
  );
};

export default RequestList;
