import { useKeycloak } from "@react-keycloak/web";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { updateBackendProfile, updateKeycloakProfile } from "../../api/profile";
import { Button } from "react-bootstrap";
import { getUserById } from "../../api/user";

const UpdateProfileForm = () => {
  //using function from react hook form so we can handle the form in an SPA way without reloading page on submit
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: "onChange" });

  //generic input validation config to assure all fields are filled out, and not filled with long spam.
  const inputLengthConfig = { required: true, maxLength: 40 };

  //local state to hold user creation errors
  const [updateProfileResponse, setUpdateProfileResponse] = useState(null);

  //local state to hold user profile information
  const [userInfo, setUserInfo] = useState(null);

  // get keycloak jwt (token)
  const { keycloak, initialized } = useKeycloak(); // todo: save token with redux?

  // get userInfo which is used for pre-filling form
  const loadUserInfo = async () => {
    const idUser = keycloak.tokenParsed.sub;
    const [result, error] = await getUserById(keycloak.token, idUser);
    setUserInfo(result);
  };
  useEffect(() => {
    if (!userInfo) {
      loadUserInfo();
    }
  }, []);

  // submit handler function to update a user with password on Keycloak and BE
  const onSubmit = async ({ firstName, lastName, email }) => {
    const userObject = { firstName, lastName, email };
    const idUser = keycloak.tokenParsed.sub;
    const [response, keycloakError] = await updateKeycloakProfile(
      userObject,
      idUser,
      keycloak.token
    );
    if (response) {
      const [backendResult, backendError] = await updateBackendProfile(
        userObject,
        idUser,
        keycloak.token
      );
      if (backendResult) {
        setUpdateProfileResponse("Profile details updated successfully");
      } else {
        setUpdateProfileResponse(backendError);
      }
    } else {
      setUpdateProfileResponse(keycloakError);
    }
  };

  return (
    <div>
      {updateProfileResponse}
          <h2>Update profile details</h2>
        {userInfo && (
          <div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <fieldset>
                <label htmlFor="firstName">First name:</label>
                <input
                  className="form-control"
                  type="text"
                  {...register("firstName", inputLengthConfig)}
                  placeholder="Limit 40 characters"
                  defaultValue={userInfo.firstName}
                />
              </fieldset>
              <fieldset>
                <label htmlFor="lastName">Last name:</label>
                <input
                  className="form-control"
                  type="text"
                  {...register("lastName", inputLengthConfig)}
                  placeholder="Limit 40 characters"
                  defaultValue={userInfo.lastName}
                />
              </fieldset>
              <fieldset>
                <label htmlFor="email">Email:</label>
                <input
                  className="form-control"
                  type="text"
                  {...register("email", inputLengthConfig)}
                  placeholder="Limit 40 characters"
                  defaultValue={userInfo.email}
                />
              </fieldset>
              <button
                  className="btn-logo btn-sep btn-logo-logo icon-edit"
                  type="submit" disabled={!isValid}>
                Update profile
              </button>
              <button
                  className="btn-logo btn-sep btn-logo-logo icon-password"
                  type="Button"
                  onClick={() => keycloak.login({ action: "UPDATE_PASSWORD" })}
              >
                Reset password
              </button>
            </form>
          </div>
        )}
    </div>
  );
};

export default UpdateProfileForm;
