import React from "react";
import NavigationBar from "./NavigationBar";
import LoginComponent from "./LoginComponent";
import { useKeycloak } from "@react-keycloak/web";
import "../../navbar.css"

//parent component holding elements of the application frame
const ApplicationFrame = () => {
  //gets access to keycloak info like token using the react keycloak adapter library
  const { keycloak, initialized } = useKeycloak();

  return (
    <div>

      {/* shows login component if not logged in */}
      {!keycloak.authenticated && <LoginComponent></LoginComponent>}

      {/* shows user's name if logged in */}
      {keycloak.authenticated && (
        <>
          <NavigationBar></NavigationBar>
          <div className="dv-welcome">
            Signed in as: {keycloak.tokenParsed.given_name}{" "}
            {keycloak.tokenParsed.family_name}{" "}
          </div> 
        </>
      )}
    </div>
  );
};

export default ApplicationFrame;
