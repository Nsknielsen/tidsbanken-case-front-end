import { useKeycloak } from "@react-keycloak/web";
import React from "react";
import { Button } from "react-bootstrap";

const LoginComponent = () => {
  //gets access to keycloak info like token using the react keycloak adapter library
  const { keycloak, initialized } = useKeycloak();

  return (
    <>
      <div className="dv-logincontainer">
    <div className="dv-loginbox">
      <div>
      <img
          src="/logo_256.png"
          alt="icon"/>
      </div>
      <div className="dv-headline">
        <h1 className={"welcome-text"}>Tidsbanken Vacation Planner</h1>
      </div>
      <div className="dv-loginbutton">
        <button className={"btn-signin"} onClick={() => keycloak.login()}>
          Sign In
        </button>
      </div>
    </div>
      </div>
    </>
  );
};

export default LoginComponent;
