import React, { useState } from "react";
import { useKeycloak } from "@react-keycloak/web";
import { addRequest } from "../../api/request";
import { useNavigate } from "react-router-dom";
import BaseRequestForm from "../../Helpers/BaseRequestForm";

const CreateRequestForm = () => {
  //navigation access to redirect to the details page for this request
  const navigate = useNavigate();

  //local state to get keycloak token for authenticating with DB when creating new request
  const { keycloak, initialized } = useKeycloak();

  //local state to tell user to select something
  const [noSelectionError, setNoSelectionError] = useState(null);

  //local state for showing data upload errors
  const [createRequestError, setCreateRequestError] = useState(null);

  //our actual form submit handler, passed through the builtin react handleSubmit()
  const onSubmit = async (requestTitle, requestComment, selection) => {
    if (selection !== null) {
      const token = keycloak.token;
      const [newRequest, error] = await addRequest(
        token,
        selection,
        requestTitle,
        requestComment
      );
      if (newRequest) {
        //nav to details page
        const requestUrl = "/request/" + newRequest.id;
        navigate(requestUrl);
      } else {
        setCreateRequestError(error);
      }
    } else {
      setNoSelectionError("Please select dates");
    }
  };

  return (
    <>
      <h2 className={"createrequest-header"}>Create a new vacation request</h2>
      <div>
        <BaseRequestForm
          submitHandler={onSubmit}
          calendarTitle="Pick dates for your request"
          formTitle="Information about your request"
          submitButtonName="Submit new request"
          includesComments={true}
          height={500}
        ></BaseRequestForm>
        {createRequestError}
      </div>
    </>
  );
};

export default CreateRequestForm;
