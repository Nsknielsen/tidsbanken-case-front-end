import { useKeycloak } from "@react-keycloak/web";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import {
  getAllBackendUsers,
  getAllKeycloakUsers,
  updateBackendAdminStatus,
} from "../../api/adminArea";
import { updateBackendProfile, updateKeycloakProfile } from "../../api/profile";

const UpdateUserForm = ({ inputLengthConfig, emailInputLengthConfig }) => {
  //react form access
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: "onChange" });

  // get keycloak jwt (token)
  const { keycloak, initialized } = useKeycloak();

  //submit handler for an updated user
  const handleSubmitForm = async () => {
    const token = keycloak.token;
    const [backendResponse, backendError] = await updateBackendProfile(
      selectedUser,
      selectedUser.id,
      token
    );
    const userObject = {
      firstName: selectedUser.firstName,
      lastName: selectedUser.lastName,
      email: selectedUser.email,
    };
    //const [backendAdminUpdate, backendAdminError] = await updateBackendAdminStatus(selectedUser.admin, selectedUser.id, token)
    const [keycloakResponse, keycloakError] = await updateKeycloakProfile(
      userObject,
      selectedUser.admin,
      selectedUser.id
    );
  };

  //get all available users on mount and put their emails into the form dropdown
  useEffect(() => {
    handleLoadUsers();
  }, []);

  //local state for the list of users
  const [users, setUsers] = useState(null);

  //local state for easy access to the currently selected user's data
  const [selectedUser, setSelectedUser] = useState(null);

  //handle load users through useEffect on mount
  const handleLoadUsers = async () => {
      const token = keycloak.token
    const [loadedUsers, error] = await getAllBackendUsers(token);
    if (loadedUsers) {
      setUsers(loadedUsers);
      console.log(loadedUsers);
    } else {
      console.log("error", error);
    }
  };

  //handle selecting a user and showing their data in the form
  const handleSelectUser = (event) => {
    const matchArray = users.filter((element) => {
      return element.id === event.target.value;
    });
    setSelectedUser(matchArray[0]);
  };

  //reactively update state then field content when user edits form. Controlled component style
  const updateFieldContent = (event, fieldName) => {
    let updatedUser = { ...selectedUser };
    switch (fieldName) {
      case "firstName":
        updatedUser.firstName = event.target.value;
        break;
      case "lastName":
        updatedUser.lastName = event.target.value;
        break;
    //   case "admin":
    //     updatedUser.admin = !updatedUser.admin;
    //     break;
      default:
        break;
    }
    setSelectedUser(updatedUser);
  };

  return (
    <div className="dv-admin-update-form">
      <h2>Update an existing user</h2>
      {users && (
        <form onSubmit={handleSubmit(handleSubmitForm)}>
          <fieldset>
            <label htmlFor="selectedUser">Select a user to update:</label>
            <select
              id="selectedUser"
              className="form-control"
              onChange={handleSelectUser}
            >
              <option selected disabled>
                Select a user from this menu
              </option>
              {users.map((user) => {
                return (
                  <option key={user.id} value={user.id}>
                    {user.email}
                  </option>
                );
              })}
            </select>
          </fieldset>
          {selectedUser && (
            <div>
              <fieldset>
                <label htmlFor="firstName">First name:</label>
                <input
                  className="form-control"
                  id="firstName"
                  type="text"
                  {...register("firstName", inputLengthConfig)}
                  placeholder="Limit 40 characters"
                  value={selectedUser.firstName}
                  onChange={(event) => updateFieldContent(event, "firstName")}
                />
              </fieldset>
              <fieldset>
                <label htmlFor="lastName">Last name:</label>
                <input
                  className="form-control"
                  id="lastName"
                  type="text"
                  {...register("lastName", inputLengthConfig)}
                  placeholder="Limit 40 characters"
                  value={selectedUser.lastName}
                  onChange={(event) => updateFieldContent(event, "lastName")}
                />
              </fieldset>
              {/* <fieldset>
                <label htmlFor="admin">Is this user an administrator?</label>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    id="admin"
                    type="checkbox"
                    {...register("admin")}
                    checked={selectedUser.admin}
                    onChange={(event) => updateFieldContent(event, "admin")}
                  />
                </div>
              </fieldset> */}
            </div>
          )}
          <button
              type="submit"
              className="btn-logo btn-sep btn-logo-logo icon-edit"
              disabled={!isValid}>
            Update user
          </button>
        </form>
      )}
    </div>
  );
};

export default UpdateUserForm;
