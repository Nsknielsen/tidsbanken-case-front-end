import { useKeycloak } from "@react-keycloak/web";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { addComment } from "../../api/comment";
import { getRequestById } from "../../api/request";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../index.css";

const AddCommentForm = ({ requestId, updateRequestInfoState, toggleCommentModal }) => {
  //local state for showing reqeust editing errors
  const [addCommentError, setAddCommentError] = useState(null);
  //keycloak access to conditionally render based on admin/not admin
  const { keycloak, initialized } = useKeycloak();

  //using function from react hook form so we can handle the form in an SPA way without reloading page on submit
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({ mode: "onChange" });

  //rules for the input form for title and comment.
  const requestCommentConfig = {
    required: true,
    maxLength: 250,
  };

  //submit handler - posts the new comment, then gets an updated requestobject
  // and passes this through the state-setting and gui-formatting in updateRequestInfoState()
  const onSubmit = async ({ newComment }) => {
    toggleCommentModal();
    const token = keycloak.token;
    const [newCommentResult, commentError] = await addComment(
      token,
      requestId,
      newComment
    );
    if (newCommentResult) {
      const [updatedRequestObject, requestError] = await getRequestById(
        token,
        requestId
      );
      if (updatedRequestObject) {
        updateRequestInfoState(updatedRequestObject);
      } else {
        setAddCommentError(requestError);
      }
    } else {
      setAddCommentError(commentError);
    }
  };

  return (
    <div className="dv-addcomment">
      {addCommentError}
      <form onSubmit={handleSubmit(onSubmit)}>
            <textarea
                style={{width: 75 + '%'}}
              className="form-control"
              {...register("newComment", requestCommentConfig)}
              placeholder="Limit 250 characters"
            />
        <button
          type="submit"
          className={"btn-logo btn-sep btn-logo-logo icon-comment"}
          style={{'margin-top': 5+'px'}}
          disabled={!isValid}
        >
          Add comment
        </button>
      </form>
    </div>
  );
};

export default AddCommentForm;
