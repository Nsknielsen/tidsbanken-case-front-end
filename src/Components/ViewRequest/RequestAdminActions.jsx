import { useKeycloak } from "@react-keycloak/web";
import React, { useState } from "react";
import {  useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { deleteRequestById, updateRequestStatus } from "../../api/request";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../calendar.css";
import "../../buttons.css";
import {
  REQUEST_STATUS_APPROVED,
  REQUEST_STATUS_DENIED,
} from "../../const/requestStatus";

const RequestAdminActions = ({ requestId, updateRequestInfoState }) => {
  //keycloak access to conditionally render based on admin/not admin
  const { keycloak, initialized } = useKeycloak();

  //access to redux state to edit and use the currently selected request
  const requestInfoState = useSelector((state) => state.viewRequest);

  //navigate access to get away after deleting this request
  const navigate = useNavigate();

  //local state for showing reqeust editing/deleting errors
  const [editOrDeleteRequestError, setEditOrDeleteRequestError] =
    useState(null);

  //handler to update request state to approved
  const handleUpdateRequest = async (requestStatus) => {
    const token = keycloak.token;
    const [updatedRequestObject, error] = await updateRequestStatus(
      token,
      requestId,
      requestStatus
    );
    updateRequestInfoState(updatedRequestObject);
  };

  const deleteRequest = async () => {
    const token = keycloak.token;
    const [deleteResponseStatus, error] = await deleteRequestById(
      token,
      requestId
    );
    if (deleteResponseStatus === 200) {
      navigate("/");
    } else {
      setEditOrDeleteRequestError(error);
    }
  };

  return (
    <>
      <button
        className="btn-logo btn-sep btn-logo-logo-approve icon-check"
        id={"push"}
        onClick={() => handleUpdateRequest(REQUEST_STATUS_APPROVED)}
        disabled={requestInfoState.requestStatus === REQUEST_STATUS_APPROVED}
      >
        Approve request
      </button>
      <button
        className="btn-logo btn-sep btn-logo-logo-deny icon-deny"
        onClick={() => handleUpdateRequest(REQUEST_STATUS_DENIED)}
        disabled={requestInfoState.requestStatus === REQUEST_STATUS_DENIED}
      >
        Deny request
      </button>
      <button className="btn-logo btn-sep btn-logo-logo icon-delete" onClick={deleteRequest}>
        Delete request
      </button>
      {editOrDeleteRequestError}
    </>
  );
};

export default RequestAdminActions;
