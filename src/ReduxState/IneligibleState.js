//state holding list of ineligibles
export const ACTION_INELIGIBLE_SET_LIST = "[ineligibleState] list of ineligible periods"

export const setIneligibleList = (ineligibleList) => {
    return {type:ACTION_INELIGIBLE_SET_LIST, payload: ineligibleList}
};

export const ineligibleReducer = (state = null, action) => {
    switch (action.type) {
        case ACTION_INELIGIBLE_SET_LIST:
            return action.payload;
        default:
            return state;
    }
};
