//state holding info about the request being viewed in ViewRequestView, formatted in a GUI-friendly way

//actions
export const ACTION_VIEWREQUEST_SET_GUI_INFO = "[viewRequestGui] set gui info";

export const setGuiRequestObjectInfo = (requestObject) => {
  return { type: ACTION_VIEWREQUEST_SET_GUI_INFO, payload: requestObject };
};

//reducer for above actions
export const viewRequestGuiReducer = (state = null, action) => {
  switch (action.type) {
    case ACTION_VIEWREQUEST_SET_GUI_INFO:
      return action.payload;
    default:
      return state;
  }
};
