//available statuses for a vacation request
export const REQUEST_STATUS_PENDING = "PENDING"
export const REQUEST_STATUS_APPROVED = "APPROVED"
export const REQUEST_STATUS_DENIED = "DENIED"
