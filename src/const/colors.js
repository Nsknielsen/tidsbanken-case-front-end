//standardized colors to use in CSS and calendar
export const COLOR_REQUEST_APPROVED = "#78b771"
export const COLOR_REQUEST_PENDING = "#f2ae54"
export const COLOR_REQUEST_DENIED = "#ff1f1f"
export const COLOR_INELIGIBLE = "#929090"