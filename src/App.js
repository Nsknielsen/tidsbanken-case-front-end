import React from "react";
import { ReactKeycloakProvider } from "@react-keycloak/web";
import keycloak from "./Keycloak";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import Dashboard from "./Views/DashboardView";
import PrivateRoute from "./Helpers/PrivateRoute";
import ApplicationFrame from "./Components/ApplicationFrame/ApplicationFrame";
import RequestHistoryView from "./Views/RequestHistoryView";
import ProfileView from "./Views/ProfileView";
import CreateRequestView from "./Views/CreateRequestView";
import AdminAreaView from "./Views/AdminAreaView";
import ViewRequestView from "./Views/ViewRequestView";

import "./App.css";



function App() {
  return (
    <div>
      {/* wrap our app in the reactkeycloak component, using our keycloak.js file as authClient */}
      <ReactKeycloakProvider authClient={keycloak}>
        <BrowserRouter>
          {/* application frame component is external to views */}
          <ApplicationFrame></ApplicationFrame>
          <Routes>
            <Route exact path="/" element={<Dashboard />} />
            {/* elements below are wrapped in the PrivateRoute component to make them only accessible to authorized users */}
            <Route
              path="/user/history"
              element={
                <PrivateRoute>
                  <RequestHistoryView></RequestHistoryView>
                </PrivateRoute>
              }
            />
            <Route
              path="/user/profile"
              element={
                <PrivateRoute>
                  <ProfileView></ProfileView>
                </PrivateRoute>
              }
            />
            <Route
              path="/create/request"
              element={
                <PrivateRoute>
                  <CreateRequestView></CreateRequestView>
                </PrivateRoute>
              }
            />
            <Route
              path="/request/:requestId"
              element={
                <PrivateRoute>
                  <ViewRequestView></ViewRequestView>
                </PrivateRoute>
              }
            />
            <Route
              path="/admin"
              element={
                <PrivateRoute>
                  <AdminAreaView></AdminAreaView>
                </PrivateRoute>
              }
            />
          </Routes>
        </BrowserRouter>
      </ReactKeycloakProvider>
    </div>
  );
}

export default App;
