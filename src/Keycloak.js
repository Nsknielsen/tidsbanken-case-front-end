import Keycloak from "keycloak-js";

//keycloak config file that allows the component in app.js to connect to the right keycloak instance
const keycloak = new Keycloak({
    url: "https://g3-keycloak.herokuapp.com/auth/",
    realm: "kc-react",
    clientId: "react-app",
});

export default keycloak;