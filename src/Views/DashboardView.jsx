import { useKeycloak } from "@react-keycloak/web";
import React, { useEffect, useState } from "react";
import DashboardCalendar from "../Components/Dashboard/DashboardCalendar";
import CreateRequestButton from "../Components/Dashboard/CreateRequestButton";
import "bootstrap/dist/css/bootstrap.min.css";
import IneligiblePeriodWindow from "../Components/Dashboard/IneligiblePeriodWindow";
import { Button } from "react-bootstrap";
import "../buttons.css";
import { getIneligibles } from "../api/ineligible";
import { setIneligibleList } from "../ReduxState/IneligibleState";
import { useDispatch } from "react-redux";
import { USER_ROLE_ADMIN } from "../const/userRoles";
import { COLOR_INELIGIBLE } from "../const/colors";

import NotificationComponent from "../Components/Dashboard/NotificationComponent";

//default public homepage
const Dashboard = () => {
  //keycloak access for conditional rendering
  const { keycloak, initialized } = useKeycloak();

  //local state for showing/hiding the modal window for ineligible periods
  const [modalIsOpen, setModalIsOpen] = useState(false);

  //Handler to open and close the modal window for ineligible periods
  const toggleModalWindow = () => {
    setModalIsOpen(!modalIsOpen);
  };

  //redux access for ineligibles
  const dispatch = useDispatch();

  //local state for errors in loading ineligibles
  const [loadIneligiblesError, setLoadIneligiblesError] = useState(null);

  //format ineligible data to be used in calendar
  const formatCalendarIneligible = (ineligible) => {
    return {
      title: `${ineligible.user.firstName} ${ineligible.user.lastName}: Ineligible Period`,
      start: ineligible.periodStart,
      end: ineligible.periodEnd,
      id: ineligible.id,
      isIneligible: true,
      overlap: false,
      backgroundColor: COLOR_INELIGIBLE,
    };
  };

  //load ineligibles on mount - used in various children through redux so it's loaded here
  const handleLoadIneligibles = async () => {
    const token = keycloak.token;
    let ineligiblesObject;
    let error;
    [ineligiblesObject, error] = await getIneligibles(token);
    //check if we have a return list of ineligibles and format them
    if (ineligiblesObject) {
      const ineligibleList = ineligiblesObject.map((ineligible) =>
        formatCalendarIneligible(ineligible)
      );
      //set ineligible periods
      dispatch(setIneligibleList(ineligibleList));
    } else {
      setLoadIneligiblesError(error);
    }
  };




  return (
    <div className={"dv-calendar"}>
      {keycloak.authenticated && (
        <>
        <NotificationComponent></NotificationComponent>
        <div className="dv-btn">
          {/* only show ineligible period-maker window to admins */}
          {keycloak.tokenParsed.roles.includes(USER_ROLE_ADMIN) && (
            <div>
              <button className={"btn-logo btn-sep btn-logo-logo icon-calendar"} onClick={toggleModalWindow}>
                Ineligible Period Dashboard
              </button>
              
              <IneligiblePeriodWindow
                modalIsOpen={modalIsOpen}
                toggleModalWindow={toggleModalWindow}
                handleLoadIneligibles={handleLoadIneligibles}
              ></IneligiblePeriodWindow>
              {loadIneligiblesError}
            </div>
          )
          }
          <CreateRequestButton></CreateRequestButton>
          </div>
          <DashboardCalendar
            handleLoadIneligibles={handleLoadIneligibles}
          ></DashboardCalendar>
        </>
      )}
    </div>
  );
};

export default Dashboard;
