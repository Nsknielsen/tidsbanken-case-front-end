import React, { useState } from "react";
import CreateUserForm from "../Components/AdminArea/CreateUserForm";
import "bootstrap/dist/css/bootstrap.min.css";
import UpdateUserForm from "../Components/AdminArea/UpdateUserForm";

const AdminAreaView = () => {
  //generic input validation config to assure all fields are filled out, and not filled with long spam.
  const inputLengthConfig = { required: true, maxLength: 40 };

  const emailInputLengthConfig = { required: true, maxLength: 100 };

  return (
    <div className="dv-adminarea">
      <CreateUserForm
        inputLengthConfig={inputLengthConfig}
        emailInputLengthConfig={emailInputLengthConfig}
      ></CreateUserForm>
      <UpdateUserForm
        inputLengthConfig={inputLengthConfig}
        emailInputLengthConfig={emailInputLengthConfig}
      ></UpdateUserForm>
    </div>
  );
};

export default AdminAreaView;
