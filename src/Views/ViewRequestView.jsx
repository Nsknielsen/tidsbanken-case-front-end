import { useKeycloak } from "@react-keycloak/web";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getCommentsByRequestId } from "../api/comment";
import "bootstrap/dist/css/bootstrap.min.css";
import { getUserByUrl } from "../api/user";
import AddCommentForm from "../Components/ViewRequest/AddCommentForm";
import EditRequestForm from "../Components/ViewRequest/EditRequestForm";
import RequestAdminActions from "../Components/ViewRequest/RequestAdminActions";
import RequestDetails from "../Components/ViewRequest/RequestDetails";
import { setGuiRequestObjectInfo } from "../ReduxState/ViewRequestGuiState";
import { setRequestObjectInfo } from "../ReduxState/ViewRequestState";
import { REQUEST_STATUS_PENDING } from "../const/requestStatus";
import { USER_ROLE_ADMIN } from "../const/userRoles";
import "../index.css";
import {Modal, ModalHeader} from "react-bootstrap";

//view for seeing details about a specific request. accessed from the dashboard calendar and request history
const ViewRequestView = () => {
  //get request id from url parameters. sent to children as prop
  const { requestId } = useParams();
  //keycloak access to conditionally render based on admin/not admin
  const { keycloak, initialized } = useKeycloak();

  //access to redux state to edit and use the currently selected request
  const dispatch = useDispatch();

  //access to redux state holding the currently selected request - conditional rendering of edit option
  const requestInfoState = useSelector((state) => state.viewRequest);

  //local state for showing/hiding the modal window for editing request
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [commentModalIsOpen, setCommentModalIsOpen] = useState(false);

  //Handler to open and close the modal window for  editing request
  const toggleModalWindow = () => {
    setModalIsOpen(!modalIsOpen);
  };
  const toggleCommentModalWindow = () => {
    setCommentModalIsOpen(!commentModalIsOpen);
  };

  //local state to hold information about whether creating user was successful
  const [getCommentInfoError, setGetCommentInfoError] = useState(null);

  //formats dates to be viewed in GUI
  const formatDatesForGui = (startDate, endDate) => {
    const formattedStartDate = new Date(startDate).toString().slice(0, 16);
    const formattedEndDate = new Date(endDate - 1).toString().slice(0, 16);
    return [formattedStartDate, formattedEndDate];
  };

  //get comment objects
  const addCommentObjects = async () => {
    //turn comment JSON getters into comment objects
    const commentToken = keycloak.token;
    const [commentObjects, commentError] = await getCommentsByRequestId(
      commentToken,
      requestId
    );
    if (commentObjects) {
      //turn the user field of the new request objects into the full object of that user for gui rendering

      for (let commentObject of commentObjects) {
        commentObject.user = await getCommentSender(commentObject);
        //convert timestamp to humanreadable format
        commentObject.timeStamp = new Date(commentObject.timeStamp)
          .toString()
          .slice(0, 21);
      }
      return commentObjects;
    } else if (commentError) {
      setGetCommentInfoError(commentError);
    }
  };

  //turn the user field of the new request objects into the full object of that user for gui rendering
  const getCommentSender = async (commentObject) => {
    const userUrl = commentObject.user;
    const userToken = keycloak.token;
    const [userObj, userError] = await getUserByUrl(userToken, userUrl);
    if (userObj) {
      return userObj;
    } else {
      setGetCommentInfoError(userError);
    }
  };

  //reformats data to be humanreadable when loading or updating a request
  const formatRequestInfoToGui = async (requestObject) => {
    let formattedRequestObject = { ...requestObject };
    [formattedRequestObject.periodStart, formattedRequestObject.periodEnd] =
      formatDatesForGui(
        formattedRequestObject.periodStart,
        formattedRequestObject.periodEnd
      );

    const commentObjects = await addCommentObjects();
    //update formatted object with new comment objects that include user details for rendering
    formattedRequestObject.comments = commentObjects;

    //return to add the updated GUI-friendly comment objects to the GUI redux state
    return formattedRequestObject;
  };

  //hack to handle having both gui-formatted and base data for the request.
  const updateRequestInfoState = async (requestObject) => {
    dispatch(setRequestObjectInfo(requestObject)); //Save base data to redux state
    const guiFormattedRequestInfo = await formatRequestInfoToGui(requestObject); // convert to human readable
    dispatch(setGuiRequestObjectInfo(guiFormattedRequestInfo)); //save gui-friendly version to redux state
  };

  //quick function to check if edit button should be disabled.
  const isEditButtonDisabled = () => {
    return (
      requestInfoState !== null &&
      requestInfoState.requestStatus !== REQUEST_STATUS_PENDING
    );
  };

  return (
    <div className="dv-ViewRequestView">
      <RequestDetails
        requestId={requestId}
        updateRequestInfoState={updateRequestInfoState}
      ></RequestDetails>
      {getCommentInfoError}
      {/* show add comment form + edit request dropdown if user is request owner OR an admin */}
      {requestInfoState !== null &&
        (requestInfoState.user.id === keycloak.tokenParsed.sub ||
          keycloak.tokenParsed.roles.includes(USER_ROLE_ADMIN)) && (
          <div className="dv-details-info">
            <button
              className="btn-logo btn-sep btn-logo-logo icon-edit"
              onClick={toggleModalWindow}
              disabled={isEditButtonDisabled()}
            >
              Edit Request
            </button>
            <button
            className="btn-logo btn-sep btn-logo-logo icon-comment"
            onClick={toggleCommentModalWindow}>
              Add Comment
            </button>
            {/* show admin actions if user is admin and is NOT the request owner*/}
            {requestInfoState !== null &&
                keycloak.tokenParsed.roles.includes(USER_ROLE_ADMIN) &&
                requestInfoState.user.id !== keycloak.tokenParsed.sub && (
                    <RequestAdminActions
                        requestId={requestId}

                        updateRequestInfoState={updateRequestInfoState}
                    ></RequestAdminActions>
                )}
          </div>
        )}
      {/* modal for commenting*/}
      {commentModalIsOpen && (
          <Modal show={commentModalIsOpen}>
            <Modal.Header closeButton onHide={toggleCommentModalWindow}>
              <Modal.Title>Add Comment</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <AddCommentForm
                  requestId={requestId}
                  updateRequestInfoState={updateRequestInfoState}
                  toggleCommentModal={toggleCommentModalWindow}
              ></AddCommentForm>
            </Modal.Body>
          </Modal>

      )}
      {/* modal window for editing request details */}
      {modalIsOpen && (
        <EditRequestForm
          modalIsOpen={modalIsOpen}
          toggleModalWindow={toggleModalWindow}
          requestId={requestId}
          updateRequestInfoState={updateRequestInfoState}
        ></EditRequestForm>
      )}
    </div>
  );
};

export default ViewRequestView;
