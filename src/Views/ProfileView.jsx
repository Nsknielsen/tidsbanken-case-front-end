import React from "react";
import UpdateProfileForm from "../Components/Profile/UpdateProfileForm";


const ProfileView = () => {
  return (
    <div className="dv-profilearea">
      <UpdateProfileForm></UpdateProfileForm>
    </div>
  );
};

export default ProfileView;
